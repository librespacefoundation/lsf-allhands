﻿
![Libre Space logo](https://i.imgur.com/fc06BHh.png)

# Claiming Space the Libre way... together

Greetings Libre Space contributors. We are pleased to host the active members of our global community pushing open space technologies forward.


# Accommodation

![King Saron Hoter](https://www.kingsaron.gr/slider/308/slider_image.jpg)Libre Space all-hands will be hosted at [King Saron Hotel](https://www.kingsaron.gr/) in Isthmia, Corinth, Greece ([Google Maps](https://www.google.com/maps/place/Hotel+King+Saron+%CE%9E%CE%B5%CE%BD%CE%BF%CE%B4%CE%BF%CF%87%CE%B5%CE%AF%CE%BF/@37.8993082,23.0038154,13.75z/data=!4m18!1m9!3m8!1s0x0:0xb79b5886fae7359!2zSG90ZWwgS2luZyBTYXJvbiDOns61zr3Ov860zr_Ph861zq_Ovw!5m2!4m1!1i2!8m2!3d37.906377!4d23.011527!3m7!1s0x0:0xb79b5886fae7359!5m2!4m1!1i2!8m2!3d37.906377!4d23.011527?hl=en-US), [Open Street Maps](https://www.openstreetmap.org/way/238077407))

# Food

Local restaurants will cater our needs for food while snacks and beverages will be available at the hotel.

# Supporters
![King Saron Hotel Logo](https://www.kingsaron.gr/img/logo.png)
King Saron Hotel supports the Libre Space all-hands meeting by providing several of it's services and facilities in reduced prices.
