# LSF All-hands meeting #

This repository contains the LSF All-hands meeting site.

## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202019-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
